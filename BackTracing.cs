﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackTracing
{
    public class Coordinate
    {
        public int X { get;set; }
        public int Y { get; set; }
        public override bool Equals(object obj)
        {
            return obj.GetType() == GetType() && ((Coordinate)obj).X == X && ((Coordinate)obj).Y == Y;
        }
    }
    public class BackTracing
    {
        public static List<Coordinate> Solve()
        {
            List<Coordinate> coordinates = new List<Coordinate>();
            if (Add(coordinates, 8))
            {
                return coordinates;
            }
            else return null;
        }
        private static bool Add(List<Coordinate> coordinates, int remaning)
        {
            if (remaning > 0)
            {
                Coordinate coordinate = new Coordinate();
                for (int i = 0; i < 8; i++)
                {
                    for (int j = 0; j < 8; j++)
                    {
                        coordinate.X = i;
                        coordinate.Y = j;
                        if (Check(coordinates, coordinate))
                        {
                            coordinates.Add(coordinate);
                            if (Add(coordinates, remaning - 1))
                            {
                                return true;
                            }
                            else
                            {
                                coordinates.Remove(coordinate);
                            }
                        }
                    }
                }
                return false;
            }
            return true;
        }
        private static bool Check(List<Coordinate> coordinates, Coordinate coordinate)
        {
            for (int i=0; i<8; i++)
            {
                if (coordinates.Contains(new Coordinate()
                {
                    X = i,
                    Y = coordinate.Y
                }))
                {
                    return false;
                }
                if (coordinates.Contains(new Coordinate()
                {
                    Y = i,
                    X = coordinate.X
                }))
                {
                    return false;
                }
                if (coordinate.X-i>=0 && coordinate.Y-i>=0 && coordinates.Contains(new Coordinate()
                {
                    X = coordinate.X-i,
                    Y = coordinate.Y-i
                }))
                {
                    return false;
                }
                if (coordinate.X + i < 8 && coordinate.Y - i >= 0 && coordinates.Contains(new Coordinate()
                {
                    X = coordinate.X + i,
                    Y = coordinate.Y - i
                }))
                {
                    return false;
                }
                if (coordinate.X - i >= 0 && coordinate.Y + i < 8 && coordinates.Contains(new Coordinate()
                {
                    X = coordinate.X - i,
                    Y = coordinate.Y + i
                }))
                {
                    return false;
                }
                if (coordinate.X + i <8  && coordinate.Y + i < 8 && coordinates.Contains(new Coordinate()
                {
                    X = coordinate.X + i,
                    Y = coordinate.Y + i
                }))
                {
                    return false;
                }
            }
            return true;
        }
    }
    
}
