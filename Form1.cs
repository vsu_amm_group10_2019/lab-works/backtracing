﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackTracing
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void solve_Click(object sender, EventArgs e)
        {
            List<Coordinate> coordinates = BackTracing.Solve();
            if (coordinates != null)
            {
                dataGridView.RowCount = 8;
                dataGridView.ColumnCount = 8;
                foreach (Coordinate coordinate in coordinates)
                {
                    dataGridView.Rows[coordinate.Y].Cells[coordinate.X].Style.BackColor = Color.Red;
                    dataGridView.Columns[coordinate.X].Width = 35;
                    
                }
            }
        }
    }
}
